# JW's D&D 5e EZ-Character-Generator
> An Expo-driven React Native project to create a D&D 5th edition 'quick start' character with the minimum information necessary to start playing

## Setup and Execution

```bash
# git clone the repo and then install dependencies in the root folder from the terminal using
npm install

> Check the package.json file for more details

# Serve with hot reload via Expo's Metro Bundler. 
# Then scan the QR code to test it on your own device.
npm start (or expo start if this doesnt work on your machine)

```

## Basic Functions

> You can choose a race, class and background

> You can share this information as plaintext to whichever applications you've allowed access within your device settings

> Future updates will reimplement image upload and saving with the plain text, manual editing of ability scores, sub-classes and more of the WotC-published races
