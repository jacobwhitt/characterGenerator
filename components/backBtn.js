import React from "react";
import { Pressable, StyleSheet, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

const BackBtn = ({ text, onPress }) => {
  const navigation = useNavigation();

  return (
    <Pressable hitSlop={20} pressRetentionOffset={{ top: 20, right: 20, bottom: 20, left: 20 }} onPress={() => navigation.navigate(onPress)} style={[styles.button, { width: text.length > 6 ? 100 : 70 }]}>
      {({ pressed }) => <Text style={styles.btnText}>{pressed ? "Loading..." : text}</Text>}
    </Pressable>
  );
};

export default BackBtn;

const styles = StyleSheet.create({
  button: {
    position: "absolute",
    top: 20,
    left: 20,
    marginTop: 20,
    width: 70,
    height: 35,
    borderRadius: 5,
    backgroundColor: '#aaaaaa77',
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
    zIndex: 2,
  },
  btnText: {
    color: "#000",
    fontSize: 18,
  },
});
