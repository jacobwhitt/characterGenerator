import React from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { Backgrounds } from "../components/backgrounds";
import BackBtn from "../components/backBtn";

const Background = (props) => {
  const backgroundNames = props.backgroundNames;

  selectBackground = (b) => {
    console.log("Background button pushed: ", b);
    const index = (element) => element.name === b;
    let backgroundIndex = Backgrounds.findIndex(index);
    props.setBackground(backgroundIndex);
  };

  return (
    <SafeAreaView style={styles.container}>
      <BackBtn text='Class' onPress='Class' color='#f3722c'></BackBtn>
      <View style={styles.main}>
        <Text style={styles.titulo}>Choose a Background:</Text>

        <FlatList
          numColumns={2}
          data={backgroundNames}
          renderItem={({ item }) => (
            <View style={styles.gridWrapper} key={item.id}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  selectBackground(`${item.name}`);
                  setTimeout(() => {
                    props.navigation.navigate("CharacterSheet");
                  }, 500);
                }}>
                <Text style={styles.text}>{item.name}</Text>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

export default Background;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  button: {
    width: 120,
    height: 40,
    backgroundColor: "#90be6d",
    borderColor: "#bc6c25",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  gridWrapper: {
    display: "flex",
    width: 100,
    height: 40,
    margin: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  main: {
    marginTop: 60,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 18,
    color: "white",
  },
  titulo: {
    color: "#90be6d",
    fontSize: 30,
  },
  subtitulo: {
    color: "brown",
    fontWeight: "bold",
    fontSize: 20,
    marginTop: 10,
  },
});
