import React, { useEffect, useState } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { ActivityIndicator, Dimensions, SafeAreaView, StyleSheet, Text, View, ScrollView, Share } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/FontAwesome";

const { width, height } = Dimensions.get("window");

const CharacterSheet = (props) => {
  const [loading, setLoading] = useState(false);
  // const [photo, setPhoto] = useState("");
  let myRace = props.user.selectedRace;
  let myClass = props.user.selectedClass;
  let myBackground = props.user.selectedBackground;
  const toolSorter = [];

  //! ARRAYS STORING HTML RENDERED IN LOOPS
  let characterStats = {
    abilityScores: [],
    armor_proficiencies: [],
    equipment: [],
    languageList: [],
    tool_proficiencies: [],
    saving_throws: [],
    skill_proficiencies: [],
    visionTypes: [],
    weapon_proficiencies: [],
    racialFeatures: [],
  };

  useEffect(() => {
    if (props.loggedIn) {
      // console.log("charSheet UE triggered");
      setLoading(true);

      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }, []);

  useFocusEffect(() => {
    if (props.loggedIn) {
      calcAbilities();
      // setPhoto(props.photoURI);
    }
  });

  //! APPLY ABILITY MODIFIERS TO STAT BLOCK
  const calcAbilities = () => {
    characterStats.abilityScores = myClass.ability_scores;

    // MODIFIERS - EACH RACE HAS 2
    let mods = Object.keys(myRace.abilitymods);
    // PREMADE STATS BLOCK FOR EACH CLASS
    let scores = Object.keys(myClass.ability_scores);

    if (props.user.statsModded === false) {
      console.log("Modifying stats!");

      mods.forEach((mod) => {
        console.log("entering mod loop", mod);
        scores.forEach((score) => {
          console.log("entering score loop", score);
          if (mod === score) {
            console.log(`${mod} is being modded from:`, myClass.ability_scores[mod], "to: ", myClass.ability_scores[mod] + myRace.abilitymods[mod]);

            characterStats.abilityScores[mod] = myClass.ability_scores[mod] + myRace.abilitymods[mod];

            console.log(`new ${mod} score: `, myClass.ability_scores[mod]);
          }
        });
      });
      console.log("running modifyStats via props");
      props.modifyStats();
    }
    console.log("end of calcAbilities");
  };

  //! SHARE ALL HTML TEXT AS PLAIN TEXT TO ANOTHER APP
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Character Name: ${props.user.name}\n\n Race: ${myRace.name}\n Class: ${myClass.name}\n Background: ${myBackground.name}\n HP: ${myClass.hit_points}\n Hit Dice: ${myClass.hit_dice}\n Languages: ${myRace.languages}\n Size: ${myRace.size}\n Speed: ${myRace.speed}\n Vision: ${
          myRace.vision
        }\n Gold: ${myBackground.gold}\n Saving Throws: ${myClass.saving_throws}\n Skill proficiencies: ${myClass.skill_proficiencies}\n Cantrips: ${myClass.cantrips}\n Spells: ${myRace.spells.length > 0 ? myRace.spells : myClass.spells}\n\n STR: ${myClass.ability_scores.STR}\n DEX: ${
          myClass.ability_scores.DEX
        }\n CON: ${myClass.ability_scores.CON}\n INT: ${myClass.ability_scores.INT}\n WIS: ${myClass.ability_scores.WIS}\n CHA: ${myClass.ability_scores.CHA}\n\nArmor proficiencies: ${myClass.armor_proficiencies}\n\nWeapon proficiencies: ${myClass.weapon_proficiencies}\n\nTool proficiencies: ${
          myClass.tool_proficiencies + ", " + myBackground.tool_proficiencies
        }\n\nStarting Equipment: ${myClass.starting_equipment}\n\nRacial Features: ${myRace.features.map((item) => `\n\n${item.name}: ${item.text}`)}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  //! LOOPING FN's TO CREATE HTML IN ARRAYS ABOVE

  // CLASS SAVING THROWS
  characterStats.saving_throws.push(myClass.saving_throws.map((item, i) => <Text key={i}>{i + 1 === myClass.saving_throws.length ? item : item + ", "}</Text>));

  // CLASS ARMOR PROFICIENCIES
  characterStats.armor_proficiencies.push(myClass.armor_proficiencies.map((item, i) => <Text key={i}>{i + 1 === myClass.armor_proficiencies.length ? item : item + ", "}</Text>));

  // CLASS WEAPON PROFICIENCIES
  characterStats.weapon_proficiencies.push(myClass.weapon_proficiencies.map((item, i) => <Text key={i}>{i + 1 === myClass.weapon_proficiencies.length ? item : item + ", "}</Text>));

  // CLASS STARTING EQUIPMENT
  characterStats.equipment.push(myClass.starting_equipment.map((item, i) => <Text key={i}>{i + 1 === myClass.starting_equipment.length ? item : item + ", "}</Text>));

  // RACIAL LANGUAGES
  characterStats.languageList.push(myRace.languages.map((item, i) => <Text key={i}>{i + 1 === myRace.languages.length ? item : item + ", "}</Text>));

  // CLASS SKILL PROFICIENCIES
  characterStats.skill_proficiencies.push(myClass.skill_proficiencies.map((item, i) => <Text key={i}>{i + 1 === myClass.skill_proficiencies.length ? item : item + ", "}</Text>));

  //! CONSOLIDATING ALL TOOL PROFICIENCIES BEFORE SETTING THE FINAL LIST
  myClass.tool_proficiencies.map((item) => {
    toolSorter.push(item);
  });
  myBackground.tool_proficiencies.map((item) => {
    if (!toolSorter.includes(item)) {
      toolSorter.push(item);
    }
  });

  //? SET FINAL TOOL PROFICIENCIES LIST TO RENDER
  characterStats.tool_proficiencies.push(toolSorter.map((item, i) => <Text key={i}>{i + 1 === toolSorter.length ? item : item + ", "}</Text>));

  // RACIAL VISION TYPES
  characterStats.visionTypes.push(myRace.vision.map((item, i) => <Text key={i}>{i + 1 === myRace.vision.length ? item : item + ", "}</Text>));

  // RACIAL FEATURES
  characterStats.racialFeatures.push(
    myRace.features.map((item, i) => (
      <View key={i}>
        <Text style={styles.nameText}>
          <Text style={styles.infoText}>{item.name}: </Text>
          {item.text}
        </Text>
      </View>
    )),
  );

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <View style={styles.loadingContainer}>
          <Text style={styles.loadingText}>SUMMONING YOUR INFORMATION</Text>
          <ActivityIndicator style={{ margin: 10 }} color='#9d0208' size='large' />
        </View>
      ) : (
        <ScrollView>
          <View style={styles.main}>
            {props.user.name.length > 15 ? <Text style={[styles.title, { fontSize: 20 }]}>{props.user.name}'s Info:</Text> : <Text style={styles.title}>{props.user.name}'s Info:</Text>}

            {/*//! CHARACTER IMAGE */}
            {/* {photo ? (
                <View style={styles.imageContainer}>
                  <Image style={styles.image} source={{ uri: photo }} />
                </View>
              ) : null} */}

            {/*//! CHARACTER INFO */}
            <View style={styles.bottom}>
              <View style={styles.characterInfo} />

              {/*//! BASIC INFO */}
              <Text style={styles.nameText}>
                <Text style={styles.sectionTitle}>Basic Info</Text>
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Race:</Text> {myRace.name}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Class:</Text> {myClass.name}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Background:</Text> {myBackground.name}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Hit Points:</Text> {myClass.hit_points}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Hit Dice:</Text> {myClass.hit_dice}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Languages:</Text> {characterStats.languageList}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Size:</Text> {myRace.size}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Speed:</Text> {myRace.speed}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Vision:</Text> {characterStats.visionTypes}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Gold:</Text> {myBackground.gold}
              </Text>

              {/* DIVIDER */}
              <View
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  borderBottomWidth: StyleSheet.hairlineWidth,
                  borderBottomColor: "#000",
                  alignSelf: "stretch",
                }}
              />

              {/*//! STATS */}
              <Text style={styles.nameText}>
                <Text style={styles.sectionTitle}>Ability Scores</Text>
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>STR:</Text> {myClass.ability_scores.STR}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>DEX:</Text> {myClass.ability_scores.DEX}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>CON:</Text> {myClass.ability_scores.CON}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>INT:</Text> {myClass.ability_scores.INT}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>WIS:</Text> {myClass.ability_scores.WIS}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>CHA:</Text> {myClass.ability_scores.CHA}
              </Text>

              {/* DIVIDER */}
              <View
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  borderBottomWidth: StyleSheet.hairlineWidth,
                  borderBottomColor: "#000",
                  alignSelf: "stretch",
                }}
              />

              {/*//! Class & Race Details  */}
              <Text style={styles.nameText}>
                <Text style={styles.sectionTitle}>Class & Race Details</Text>
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Cantrips:</Text> {myClass.cantrips}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Spells:</Text> {myRace.spells.length > 0 ? myRace.spells : myClass.spells}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Saving Throws:</Text> {characterStats.saving_throws}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Skill Proficiencies:</Text> {characterStats.skill_proficiencies}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Armor Proficiencies:</Text> {characterStats.armor_proficiencies}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Weapon Proficiencies:</Text> {characterStats.weapon_proficiencies}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Tool Proficiencies:</Text> {characterStats.tool_proficiencies}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Starting Equipment:</Text> {characterStats.equipment}
              </Text>
              <Text style={styles.nameText}>
                <Text style={styles.infoText}>Racial Features:</Text>
              </Text>
              {characterStats.racialFeatures}
            </View>
          </View>
        </ScrollView>
      )}

      {/*//! SUBMISSION/RESET BUTTONS */}
      {loading ? null : (
        <View style={styles.submissionContainer}>
          <TouchableOpacity
            style={styles.resetButton}
            onPress={() => {
              props.logout();
              props.navigation.navigate("Home");
            }}>
            <Text style={[styles.btnText, { color: "#9d0208" }]}>Start Over?</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.saveButton}
            onPress={() => {
              onShare();
            }}>
            <Text style={styles.btnText}>Share</Text>
            <Icon name='share' size={30} color='#fff' />
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
};

export default CharacterSheet;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  bottom: {
    flexDirection: "column",
    alignItems: "flex-start",
    width: "80%",
  },
  btnText: {
    textAlign: "center",
    fontSize: 20,
    // },
    color: "#fff",
  },
  characterInfo: {
    marginBottom: 10,
    alignSelf: "stretch",
  },
  // imageContainer: {
  //   flexDirection: "row",
  //   width: "100%",
  //   marginTop: 20,
  //   alignItems: "center",
  //   justifyContent: "center",
  // image: {
  //   width: 300,
  //   height: 300,
  //   resizeMode: "contain",
  // },
  infoText: {
    color: "#9d0208",
    fontSize: 20,
    lineHeight: 30,
    textAlign: "left",
  },
  loadingContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  loadingText: {
    textAlign: "center",
    color: "#9d0208",
    fontSize: 24,
  },
  main: {
    alignItems: "center",
    justifyContent: "center",
  },
  nameText: {
    color: "#000",
  },
  resetButton: {
    width: 150,
    height: 40,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#9d020850", // IOS
    shadowOffset: { height: 2, width: 2 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  saveButton: {
    flexDirection: "row",
    width: 150,
    height: 40,
    backgroundColor: "#90be6d",
    borderColor: "#bc6c25",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "space-evenly",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  sectionTitle: {
    color: "#f00000",
    fontSize: 24,
  },
  submissionContainer: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  title: {
    fontSize: 30,
    color: "#9d0208",
    textAlign: "center",
  },
});
