import React, { useState } from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { Classes } from "../components/classes";
import BackBtn from "../components/backBtn";

const Class = (props) => {
  const classNames = props.classNames;

  selectClass = (c) => {
    console.log("Class button pushed: ", c);
    const index = (element) => element.name === c;
    let classIndex = Classes.findIndex(index);
    props.setClass(classIndex);
  };

  return (
    <SafeAreaView style={styles.container}>
      <BackBtn text='Race' onPress='Race' color='#d00000'></BackBtn>
      <View style={styles.main}>
        <Text style={styles.titulo}>Choose a Class:</Text>

        <FlatList
          numColumns={2}
          data={classNames}
          renderItem={({ item }) => (
            <View style={styles.gridWrapper} key={item.id}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  selectClass(`${item.name}`);
                  props.navigation.navigate("Background");
                }}>
                <Text style={styles.text}>{item.name}</Text>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

export default Class;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  main: {
    marginTop: 60,
    alignItems: "center",
    justifyContent: "center",
  },
  titulo: {
    color: "#f3722c",
    fontSize: 30,
  },
  subtitulo: {
    color: "brown",
    fontWeight: "bold",
    fontSize: 20,
    marginTop: 10,
  },
  text: {
    fontSize: 18,
    color: "white",
  },
  button: {
    width: 120,
    height: 40,
    backgroundColor: "#f3722c",
    borderColor: "#bc6c25",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  gridWrapper: {
    // backgroundColor: "red",
    display: "flex",
    width: 100,
    height: 40,
    margin: 15,
    alignItems: "center",
    justifyContent: "center",
  },
});
