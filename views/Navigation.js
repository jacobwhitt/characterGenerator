import React, { useState } from "react";
import { Dimensions, SafeAreaView, StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "./Home";
import Race from "./Race";
import Class from "./Class";
import Background from "./Background";
// import Photo from "./Photo";
import CharacterSheet from "./CharacterSheet";

const Stack = createStackNavigator();
const { width, height } = Dimensions.get("window");

const Navigation = (props) => {
  let navProps = props;
  // const [photoURI, setPhotoURI] = useState("");
  // const saveUploadedPhoto = (photo) => {
  //   setPhotoURI(photo);
  // };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}>
      <Stack.Screen name='Home'>
        {(props) => <Home {...props} errorMessage={navProps.errorMessage} login={navProps.login} logout={navProps.logout} loggedIn={navProps.loggedIn} modifyStats={navProps.modifyStats} name={navProps.user.name} readStorage={navProps.readStorage} setName={navProps.setName} user={navProps.user} />}
      </Stack.Screen>

      <Stack.Screen name='Race'>{(props) => <Race {...props} raceNames={navProps.raceNames} modifyStats={navProps.modifyStats} setRace={navProps.setRace} />}</Stack.Screen>

      <Stack.Screen name='Class'>{(props) => <Class {...props} classNames={navProps.classNames} modifyStats={navProps.modifyStats} setClass={navProps.setClass} />}</Stack.Screen>

      <Stack.Screen name='Background'>{(props) => <Background {...props} backgroundNames={navProps.backgroundNames} setBackground={navProps.setBackground} />}</Stack.Screen>

      {/* <Stack.Screen name='Photo'>{(props) => <Photo {...props} saveUploadedPhoto={saveUploadedPhoto} />}</Stack.Screen> */}

      <Stack.Screen name='CharacterSheet'>{(props) => <CharacterSheet {...props} user={navProps.user} loggedIn={navProps.loggedIn} modifyStats={navProps.modifyStats} logout={navProps.logout} />}</Stack.Screen>
    </Stack.Navigator>
  );
};

export default Navigation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    width: width,
    height: height,
  },
  main: {
    width: width,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: "80%",
    height: 100,
    resizeMode: "contain",
    alignSelf: "center",
  },
  titulo: {
    color: "#d00000",
    fontSize: 40,
    fontWeight: "bold",
  },
  subtitulo: {
    marginTop: 10,
    color: "#d00000",
    fontSize: 30,
    fontWeight: "bold",
  },
  input: {
    marginTop: 20,
    height: 50,
    width: 200,
    borderWidth: 1,
    borderColor: "#ccc",
    alignItems: "center",
    justifyContent: "center",
  },
  bodyText: {
    width: "80%",
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#9d0208",
    textAlign: "center",
    lineHeight: 40,
    fontSize: 24,
  },
  subtext: {
    width: "80%",
    textAlign: "center",
    lineHeight: 20,
    fontSize: 12,
  },
  button: {
    marginTop: 20,
    width: 200,
    height: 60,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  logout: {
    position: "absolute",
    top: 20,
    left: 20,
    marginTop: 20,
    width: 70,
    height: 35,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  btnText: {
    color: "white",
    fontSize: 24,
  },
});
